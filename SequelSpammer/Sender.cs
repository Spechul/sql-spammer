﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequelSpammer
{
    class Sender : IDisposable
    {
        private int _numberOfChannels;
        private SqlConnection _connection;
        private SqlDataAdapter _insertAdapter;

        public Sender(string connectionString, int numberOfChannels)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _numberOfChannels = numberOfChannels;
        }

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }

        public void Insert()
        {
            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            Random r = new Random();

            for (int i = 0; i < _numberOfChannels; i++)
            {
                SqlCommand insert = new SqlCommand();
                insert.CommandText = string.Format("INSERT INTO dbo.MEASUREMENTS ([TStamp], [Routing], [MeasureValue]) VALUES (DEFAULT, 'Depth{0}', {1})", i, ((float)r.NextDouble()).ToString("0.000000000"));//{i}', {r.NextDouble()})";
                //Console.WriteLine(insert.CommandText);
                insert.Connection = _connection;
                insert.ExecuteNonQuery();
            }

            _connection.Close();
        }
    }
}
