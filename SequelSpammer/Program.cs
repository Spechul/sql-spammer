﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SequelSpammer
{
    class Program
    {
        #region Configure here

        private const string ConnectionString = "Server=(localdb)\\mssqllocaldb;Database=master;Trusted_Connection=True;MultipleActiveResultSets=true";
        private const int MillisecondsPeriod = 1000;
        private const int NumberOfChannels = 10;

        #endregion


        // milliseconds
        private const int RepeatConnectionIn = 200;

        private static CancellationTokenSource cts;
        private static CancellationToken token;

        static void Main(string[] args)
        {
            // need to correctly convert double -> string
            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;

            cts = new CancellationTokenSource();
            token = cts.Token;

            Console.WriteLine("press enter to start inserting. second time pressing will stop inserting");
            Console.ReadLine();

            Task.Factory.StartNew(() => startSpam(), token);

            Console.ReadLine();
            cts.Cancel();
            //some time to surely finish thread
            Thread.Sleep(500);
            Console.WriteLine("press enter to close app");
            Console.ReadLine();
        }

        private static void startSpam()
        {
            Sender s;

            // trying to set up connection
            while ((s = TrySetupConnection()) == null)
            {
                if (token.IsCancellationRequested)
                    return;
            }

            // sending
            using (s)
            {
                Console.WriteLine("inserting");
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("cancelled");
                        return;
                    }

                    try
                    {
                        s.Insert();
                    }
                    catch (Exception e)
                    when (e is SqlException || e is NullReferenceException)
                    {
                        Console.WriteLine("database is not responding");
                        s.Dispose();
                        s = TrySetupConnection();
                        Thread.Sleep(RepeatConnectionIn);
                    }

                    Thread.Sleep(MillisecondsPeriod);
                }
            }
        }

        private static Sender TrySetupConnection()
        {
            Sender s = null;
            if (token.IsCancellationRequested)
                return s;

            try
            {
                s = new Sender(ConnectionString, NumberOfChannels);
                return s;
            }
            catch (SqlException e)
            {
                Console.WriteLine("database is not responding");
                Thread.Sleep(RepeatConnectionIn);
                return s;
            }
        }
    }
}
